#!/bin/bash

PATH_GENERATOR="generator-jhipster-blog"
GENERATOR_NAME="generator-jhipster-blog"
PATH_CORE="jhipster-core"
PATH_TEST="jhipster-temp"

start() {
    echo "####################### START #######################"
    echo "This script required : npm, Yeoman, jvm"
}

end() {
    echo "####################### END #######################"
    exit 0
}

run() {
    start
    installDependenciesCore ${PATH_CORE}
    installDependenciesCore ${PATH_TEST}
    installDependencies ${PATH_GENERATOR}
    createDevLink
    deployModuleForTestInstallation
    echo " "
    echo "========================================================================="
    echo "                   AND NOW?                                              "
    echo "========================================================================="
    echo " "
    echo "[INFO] Now you can try this module :"
    echo "             cd jhipster-temp && ./mvnw"
    echo "             cd jhipster-temp && npm start"
    echo " "
    echo "[INFO] Or develop in folder ./jhipster-core :"
    echo "             cd jhipster-core && ./mvnw"
    echo "             cd jhipster-core && npm start"
    echo " "
    echo "[INFO] Or update installation workflow on folder ./jhipster-core and ./build.sh"
    echo " "
    echo "========================================================================="
    echo " "
    end
}

deployModuleForTestInstallation() {
    # deploy last dev on module
    ./build.sh
    cd ${PATH_TEST}
    # install module on test environment
    yo ${GENERATOR_NAME}
    cd ..
}

createDevLink() {
    cd ${PATH_GENERATOR} && npm link
    cd ..
    cd ${PATH_TEST} && npm link ${GENERATOR_NAME}
    cd ..
}

#
# Install npm and mvn dependencies.
#
installDependencies() {
    cd $1 && npm install
    cd ..
}
installDependenciesCore() {
    installDependencies $1
	cd $1 && ./mvnw compile
	cd ..
}

run
