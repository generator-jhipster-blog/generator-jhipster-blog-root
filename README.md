# generator-jhipster-blog
[![NPM version][npm-image]][https://www.npmjs.com/package/generator-jhipster-blog]
> JHipster module, Quick and simple blog for JHipster

> This project content all sub-projects for JhiptserBlogModule

# Introduction

This is a [JHipster](https://www.jhipster.tech/) module, that is meant to be used in a JHipster application.

# Usage instructions
For use this JHipster module see :
* Repository : https://framagit.org/generator-jhipster-blog/generator-jhipster-blog
* Documentation : https://framagit.org/generator-jhipster-blog/generator-jhipster-blog/blob/master/README.md
* Marketplace : https://www.jhipster.tech/modules/marketplace/#/details/generator-jhipster-blog

# Contribute instructions

## Repositories instructions
* `generator-jhipster-blog-root` : this is the root project with git sub modules
  * `generator-jhipster-blog` : this is NPM repository with final code (template for module installation)
  * `jhipster-core` : this is a Jhipster project with Blog module code source
  * `jhipster-temp` : this is a Jhipster project keep cleaning just for test module installation

## Install dev. environment
* Clone this repository and this sub-modules :
  ```bash
  git clone --recurse-submodules -j4 https://framagit.org/generator-jhipster-blog/generator-jhipster-blog-root
  ```
* Go in root folder : 
  ```bash
  cd generator-jhipster-blog-root
  ```
* Allow scripts executions : 
  ```bash
  chmod +x ./build.sh ./install-dev.sh
  ```
* Install dev. environment : 
  ```bash
  ./install-dev.sh
  ```
* Code in projet `jhipster-core` (see "Importance notice")
* Build Jhipster module : 
  ```bash
  ./build.sh
  ```
* Test module installation : 
  ```bash
  cd `jhipster-temp` && yo jhipster-blog
  ```

## Important notice
* Do not commit on project `jhipster-temp`, this project is keep cleaning. Use `git revert` for multiples installations tests.
* If you add some files on this module you need to update two files : 
  * `build.sh` : Include your files in this script for build final module in `generator-jhipster-blog`
  * `generator-jhipster-blog/generators/app/index.js` : Include your deployment instruction

# Deploymnt
* `jhipster-core` : Create and finish new release with git-flow
* Running build script : `./build.sh`
* `generator-jhipster-blog` : `npm run release:YOUR_VERSION_TYPE` 

# License

MIT © [Gael S.](http://sigogneau.fr)


[npm-image]: https://img.shields.io/npm/v/generator-jhipster-blog.svg
[npm-url]: https://npmjs.org/package/generator-jhipster-blog
[travis-image]: https://travis-ci.org/gantispam/generator-jhipster-blog.svg?branch=master
[travis-url]: https://travis-ci.org/gantispam/generator-jhipster-blog
[daviddm-image]: https://david-dm.org/gantispam/generator-jhipster-blog.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/gantispam/generator-jhipster-blog
