#!/bin/bash

JHI_CORE="jhipster-core"
PATH_GENERATOR="generator-jhipster-blog"
PATH_TEMPLATE="${PATH_GENERATOR}/generators/app/templates/src/main"
PACKAGE="com.gsig.videoapp"
PATH_PACKAGE="com/gsig/videoapp"

start() {
    echo "####################### START #######################"
}

end() {
    echo "####################### END #######################"
    exit 0
}

run() {
    start
	workOnGitDevelop
    runJavaPackage
    runResources
    runWebapp
    end
}

#
# work on branch master
#
workOnGitDevelop() {
	cd ${PATH_GENERATOR} && git checkout master
	cd ..
}

#
# Install java packages.
#
runJavaPackage() {
    echo "### Install java packages"
    installPackage "constant"
    installPackage "domain/blog"
    installPackage "repository/blog"
    installPackage "service/blog"
    installPackage "web/rest/blog"
}

#
# Install resources.
#
runResources() {
    echo "### Install ressources"
    installLiquibase "blog"
    installDependencies "resources/config/liquibase/fake-data/blog"
}

#
# Install webapp.
#
runWebapp() {
    echo "### Install webapp"
    installI18n "webapp/i18n/en/blog.json"
    installI18n "webapp/i18n/fr/blog.json"
    installWebapp "webapp/app/blog"
}

#
# @param package's name.
#
installPackage() {
    SOURCE=${JHI_CORE}/src/main/java/${PATH_PACKAGE}/$1
    TARGET=${PATH_TEMPLATE}/java/package/$1
    echo "move ${SOURCE} on ${TARGET}"
    # clear folder
    rm -rf ${TARGET}
    # create root folder
    mkdir -p $(dirname ${TARGET})
    # copy folder
    cp -r ${SOURCE} ${TARGET}
    # rename java class
	find ${TARGET} -type f -iname '*.java' -exec bash -c 'mv $0 $(dirname $0)/_$(basename ${0})' {} \;
    # rename package on java class
    find ${TARGET} -type f -exec sed -i 's/'"${PACKAGE}"'/<%=packageName%>/g' {} \;
}

#
# @param liquibase changelog folder to install.
#
installLiquibase() {
	TARGET="resources/config/liquibase/changelog/$1"
    installDependencies ${TARGET}
    # rename changelog
    find ${TARGET} -type f -exec sed -i 's/20191020132828/<%=changelogDate%>/g' {} \;
}

#
# @param module path.
#
installWebapp() {
	SOURCE=$1
	installDependencies ${SOURCE}
    # rename jhipster module importation
    find ${TARGET} -type f -iname '*.ts' -exec sed -i 's/JhipsterSharedModule/<%= locals.microserviceName ? upperFirstCamelCase(locals.microserviceName) : angularXAppName %>SharedModule/g' {} \;
    # rename i18n reference
	find ${TARGET} -type f -iname '*.html' -exec sed -i 's/jhipsterApp./<%= angularAppName %>./g' {} \;
	find ${TARGET} -type f -iname '*.ts' -exec sed -i 's/jhipsterApp./<%= angularAppName %>./g' {} \;
}

#
# @param module path.
#
installI18n() {
	SOURCE=$1
	installDependencies ${SOURCE}
    # rename i18n reference
    find ${TARGET} -type f -iname '*.json' -exec sed -i 's/jhipsterApp/<%= angularAppName %>/g' {} \;
}

#
# @param file or folder to install.
#
installDependencies() {
    SOURCE=${JHI_CORE}/src/main/$1
    TARGET=${PATH_TEMPLATE}/$1
    echo "move ${SOURCE} on ${TARGET}"
    # clear folder
    rm -rf ${TARGET}
    # create root folder
    mkdir -p $(dirname ${TARGET})
    # copy folder
    cp -r ${SOURCE} ${TARGET}
}

run
